import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-componente3',
  templateUrl: './componente3.component.html',
  styleUrls: ['./componente3.component.css']
})
export class Componente3Component implements OnInit {
 // toppings = new FormControl();

 // toppingList: string[] = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];

  constructor() { }

  myForm = new FormGroup(
    {
      username: new FormControl('', Validators.required),
      password: new FormControl('',Validators.required)
    }
  )

  ngOnInit(): void {
  }
  onSudmit(){
    alert("Usuario " + this.myForm.get('username')?.value);
    this.myForm.reset();
  }
}
