import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Mfe2Component } from './mfe2.component';
import { Mfe2RoutingModule } from './mfe2-routing.module';
import {MatToolbarModule} from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule} from '@angular/material/card';
import { Componente1Component } from './componente1/componente1.component';
import { Componente2Component } from './componente2/componente2.component';
import { Componente3Component } from './componente3/componente3.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    Mfe2Component,
    Componente1Component,
    Componente2Component,
    Componente3Component
    
  ],
  imports: [
    CommonModule,
    Mfe2RoutingModule,
    MatToolbarModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  
    
  ],
  exports:[Mfe2Component]
})
export class Mfe2Module { }
