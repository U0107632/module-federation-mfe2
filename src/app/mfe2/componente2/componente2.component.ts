import { Component, OnInit } from '@angular/core';
import '@angular/localize/init';

@Component({
  selector: 'app-componente2',
  templateUrl: './componente2.component.html',
  styleUrls: ['./componente2.component.css']
})
export class Componente2Component implements OnInit {
  images = [944, 1011, 984].map((n) => `https://picsum.photos/id/${n}/900/500`);
  constructor() {
    
   }

  ngOnInit(): void {
   

  }

}
