import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Componente1Component } from './componente1/componente1.component';
import { Componente2Component } from './componente2/componente2.component';
import { Componente3Component } from './componente3/componente3.component';
import { Mfe2Component } from './mfe2.component';

const routes: Routes = [
  {
    path:'',
    component: Mfe2Component,
    children:[
      {
        path:'',
        component: Componente1Component
      },
      {
        path:'dashboard',
        component: Componente1Component
      },
      {
        path:'carrousel',
        component: Componente2Component
      },
      {
        path:'form',
        component: Componente3Component
      }
    ]
  },

 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Mfe2RoutingModule { }
